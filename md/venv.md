--- venv ---
  python3 -m venv circle_venv

  source circle_venv/bin/activate

  python3 -m pip install -U pylint

  pip freeze > requirements.txt

  .

  pip install -r requirements.txt

  https://machine-learning-blog.de/2019/09/13/virtuelle-umgebung-venv-fur-die-entwicklung-mit-python-einrichten-und-sichern/

  https://medium.com/bettercode/how-to-build-a-modern-ci-cd-pipeline-5faa01891a5bhttps://stackoverflow.com/questions/12277933/send-data-from-a-textbox-into-flask

  https://www.youtube.com/watch?v=ylYANOrMatI
  https://www.youtube.com/watch?v=ia0rvIfxPFc


--- GitLab Runner ---
  sudo apt-get install gitlab-runner

  sudo service gitlab-runner status
  (check)

  sudo gitlab-runner register

  (Settings > CI/CD and expand the Runners section)
  $ Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
  https://gitlab.com

  § Please enter the gitlab-ci token for this runner:
  -jEWNz7th7tEQ3pFa2M7

--- Docker installation ---
https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru
https://dockerswarm.rocks/gitlab-ci/

sudo docker ps

sudo docker logs gitlab-runner

sudo docker logs gitlab-runner -f

sudo docker ps

