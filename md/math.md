def add(x, y=2):
    return x + y


def product(x, y=2):
    return x * y

---TEST

import math_functions


def test_add():
    assert math_functions.add(7, 3) == 10
    assert math_functions.add(7) == 9


def test_product():
    assert math_functions.product(7, 3) == 21
    assert math_functions.product(7) != 7
    assert math_functions.product(7) == 14
