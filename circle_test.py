"testing circle_area method"

import unittest
from math import pi
from circle import circle_area


class TestCircleArea(unittest.TestCase):
    "bitte schön eine docstring"

    def test_area(self):
        "test if formula is correct"

        self.assertEqual(circle_area(3), pi*3**2)
        self.assertEqual(circle_area(1), pi)
        self.assertEqual(circle_area(0), 0)
        self.assertEqual(circle_area(2.5), pi*2.5**2)

    def test_values(self):
        "raise error if value is negativ"
        self.assertEqual(circle_area(-2.5), "Radius can't be negative")

    def test_types(self):
        "raise error if value is NaN"
        self.assertEqual(circle_area("ten"), "It wasn't a number")
