""" it calculates a circle area """

from math import pi


def circle_area(radius):
    """ it calculates a circle area """

    if type(radius) not in [int, float]:
        return "It wasn't a number"
    if radius < 0:
        return "Radius can't be negative"

    return pi * radius ** 2
