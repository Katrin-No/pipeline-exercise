""" simple page """

import os
import re
from flask import Flask, render_template, request
from circle import circle_area

app = Flask(__name__)


@app.route("/")
def main():
    """ renders index template """
    return render_template("index.html")


@app.route("/send", methods=["POST"])
def send():
    """ start pulling data from form input """
    number = request.form["radius"]
    if request.method == "POST":
        if not re.compile(r"^\-?\d+(\.\d+)?$").match(request.form["radius"]):
            return render_template("index.html", result="It wasn't a number", number=number)
        radius = float(request.form["radius"])
        result = circle_area(radius)
        return render_template("index.html", result=result, number=number)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=int(os.getenv('PORT', 5000)))
